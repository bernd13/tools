# Module tools

Helper functions for several purposes.

# Package de.praxisit.tools.collection

## Purpose 

Extension functions for collections.

## Split

Functions that split a list of elements in several ways.

## Reduce

Functions that reduce lists in different ways.