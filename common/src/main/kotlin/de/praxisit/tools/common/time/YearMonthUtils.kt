package de.praxisit.tools.common.time

import java.time.YearMonth

/** Check if [month] is the month before current [YearMonth]].
 * @receiver YearMonth current month
 * @param month YearMonth possible the month before
 * @return Boolean `true` if [month] is the month before, else `false`
 */
public infix fun YearMonth.predecessorOf(month: YearMonth): Boolean = month.minusMonths(1L) == this

/** Check if [month] is the month after current [YearMonth]].
 * @receiver YearMonth current month
 * @param month YearMonth possible the month after
 * @return Boolean `true` if [month] is the month after, else `false`
 */
public infix fun YearMonth.successorOf(month: YearMonth): Boolean = month.plusMonths(1L) == this

/**The earliest month.
 * @param month1 YearMonth first month
 * @param month2 YearMonth second month
 * @return YearMonth either [month1] or [month2]
 */
public fun min(month1: YearMonth, month2: YearMonth): YearMonth = if (month1 < month2) month1 else month2

/**The latest month.
 * @param month1 YearMonth first month
 * @param month2 YearMonth second month
 * @return YearMonth either [month1] or [month2]
 */
public fun max(month1: YearMonth, month2: YearMonth): YearMonth = if (month1 > month2) month1 else month2
