@file:Suppress("unused")

package de.praxisit.tools.common.text

public fun String.addSuffix(suffix: String): String = if (this.endsWith(suffix)) this else this + suffix

public fun String.addSuffix(suffix: CharSequence): String = if (this.endsWith(suffix)) this else this + suffix
