@file:Suppress("RemoveRedundantQualifierName", "RedundantVisibilityModifier")

package de.praxisit.tools.common.time

import java.time.YearMonth
import java.time.format.DateTimeFormatter

/**
 * Range of months.
 *
 * @property start YearMonth start of range
 * @property endInclusive YearMonth included end of range
 * @constructor creates a YearMonthRange
 */
public class YearMonthRange private constructor(
    public val start: YearMonth,
    public val endInclusive: YearMonth
) : Iterable<YearMonth> {

    public operator fun contains(month: YearMonth): Boolean = (month >= start) && (month <= endInclusive)

    public override fun equals(other: Any?): Boolean = when {
        other == null -> false
        other === this -> true
        else -> other is YearMonthRange && other.start == this.start && other.endInclusive == endInclusive
    }

    public override fun hashCode(): Int = start.hashCode() xor endInclusive.hashCode()

    public override fun toString(): String =
        "${start.format(YEAR_MONTH_FORMAT)} - ${endInclusive.format(YEAR_MONTH_FORMAT)}"

    public override fun iterator(): Iterator<YearMonth> = object : Iterator<YearMonth> {
        private var month = start

        override fun hasNext() = month <= endInclusive

        override fun next(): YearMonth {
            val next = month
            if (next > endInclusive) throw NoSuchElementException()
            month = month.plusMonths(1L)
            return next
        }
    }

    public operator fun plus(month: YearMonth): YearMonthRange = when {
        month predecessorOf start -> YearMonthRange(month, endInclusive)
        month successorOf endInclusive -> YearMonthRange(start, month)
        else -> this
    }

    public operator fun plus(other: YearMonthRange): YearMonthRange = when {
        this.endInclusive predecessorOf other.start -> YearMonthRange(this.start, other.endInclusive)
        this.start successorOf other.endInclusive -> YearMonthRange(other.start, this.endInclusive)
        this overlap other -> YearMonthRange(min(this.start, other.start), max(this.endInclusive, other.endInclusive))
        else -> this
    }

    private infix fun extensionPossibleBy(month: YearMonth) =
        this.contains(month) || month successorOf endInclusive

    /**
     * Check if 2 ranges overlap each other.
     *
     * @param other YearMonthRange the other range
     * @return Boolean `true` if some months are in this YearMonthRange and the `other`
     */
    public infix fun overlap(other: YearMonthRange): Boolean =
        (this.endInclusive >= other.start && this.start <= other.endInclusive)

    public companion object {
        private val YEAR_MONTH_FORMAT = DateTimeFormatter.ofPattern("MM.yyyy")

        /**
         * Creates a valid [YearMonthRange].
         *
         * @param start YearMonth lesser than `endInclusive`
         * @param endInclusive YearMonth greater or equal to `start`. The end is included.
         *        The default value is the `start`.
         * @return YearMonthRange range of months
         * @throws IllegalArgumentException if `start` > `endInclusive`
         */
        public fun of(start: YearMonth, endInclusive: YearMonth = start): YearMonthRange {
            if (start > endInclusive) throw IllegalArgumentException("start ($start) > end($endInclusive)")
            return YearMonthRange(start, endInclusive)
        }

        /**
         * Compress a set of [YearMonth]s to a sorted list of [YearMonthRange].
         *
         * @receiver Set<YearMonth> a set of [YearMonth]s
         * @return List<YearMonthRange> the shortest list of [YearMonthRange]s that contains all months of the set
         */
        public fun Set<YearMonth>.compress(): List<YearMonthRange> {
            if (this.isEmpty()) return emptyList()

            val sortedMonths = this.sorted()
            val result = mutableListOf<YearMonthRange>()
            var currentRange = YearMonthRange.of(sortedMonths.first())

            sortedMonths.forEach { month ->
                val range = currentRange
                currentRange =
                    if (range extensionPossibleBy month) range + month
                    else {
                        result.add(range)
                        YearMonthRange.of(month)
                    }
            }
            result.add(currentRange)
            return result
        }
    }
}
