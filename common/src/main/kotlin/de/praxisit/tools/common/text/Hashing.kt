package de.praxisit.tools.common.text

import java.security.MessageDigest

/**
 * Hashing offers simple extensions to apply hash algorithms to strings.
 */
public object Hashing {
    /** Converts a string to its MD5 representation. */
    public val String.md5: String
        get() = MD5.digest(this.toByteArray()).bytesToHex

    /** Converts a string to its SHA-1 representation. */
    public val String.sha1: String
        get() = SHA1.digest(this.toByteArray()).bytesToHex

    /** Converts a string to its SHA-256 representation. */
    public val String.sha256: String
        get() = SHA256.digest(this.toByteArray()).bytesToHex

    private val MD5: MessageDigest = MessageDigest.getInstance("MD5")
    private val SHA1: MessageDigest = MessageDigest.getInstance("SHA-1")
    private val SHA256: MessageDigest = MessageDigest.getInstance("SHA-256")

    private val ByteArray.bytesToHex
        get() = joinToString("") { "%02x".format(it) }
}
