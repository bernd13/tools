package de.praxisit.tools.common.value

public enum class Gender {
    MALE,
    FEMALE,
    DIVERSE
}
