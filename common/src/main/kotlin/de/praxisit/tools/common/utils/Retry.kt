package de.praxisit.tools.common.utils

/**
 * Retry the [block] closure
 * @param tries Int
 * @param block Function0<T>
 * @return T
 */
public fun <T> retry(tries: Int = DEFAULT_TRIES, block: () -> T): T {
    lateinit var lastException: Exception

    for (i in 1..tries) {
        try {
            return block()
        } catch (@SuppressWarnings("detekt:TooGenericExceptionCaught") ex: RuntimeException) {
            lastException = ex
        }
    }

    throw lastException
}

public const val DEFAULT_TRIES: Int = 10
