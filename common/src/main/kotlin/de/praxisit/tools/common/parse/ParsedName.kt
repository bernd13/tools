package de.praxisit.tools.common.parse

public data class ParsedName(val firstName: String?, val lastName: String?, val title: String?) {
    public companion object {
        /**
         * Recognizes a [ParsedName] from a text.
         *
         * The method recognizes
         * - titles (*Prof.* and *Dr.*),
         * - multiple first names,
         * - double first names like Hans-Josef,
         * - double last names like Müller-Schulz,
         * - nobility titles like 'von der' und 'zu'
         * - simple genealogical add ons like *jr.* or *sr.*.
         *
         * When the text is `null` or empty `null` is returned.
         *
         * Spaces at the beginning or end will be ignored.
         *
         * @return [ParsedName]: `null` if the string does not contain a name
         */
        public fun String?.parseName(): ParsedName? = this?.let { text ->
            fun String?.emptyToNull() = if (this.isNullOrBlank()) null else this.trim()
            NAME_PATTERN.matchEntire(text.trim { it <= ' ' })?.let { matchResult ->
                ParsedName(
                    firstName = matchResult.groups[FIRST_NAME_GROUP]?.value?.emptyToNull(),
                    lastName = matchResult.groups[LAST_NAME_GROUP]?.value?.emptyToNull(),
                    title = matchResult.groups[TITLE_GROUP]?.value?.emptyToNull(),
                )
            }
        }

        private const val TITLE_GROUP = "title"
        private const val FIRST_NAME_GROUP = "firstname"
        private const val LAST_NAME_GROUP = "lastname"

        @JvmStatic
        @SuppressWarnings("MaxLineLength")
        private val NAME_PATTERN =
            ("""^(?<$TITLE_GROUP>(Prof\.\s+)?(Dr\.\s+)*)""" +
                    """(?<$FIRST_NAME_GROUP>(\p{Lu}\p{Ll}+(-\p{Lu}\p{Ll}+)?\s+)*)""" +
                    """(?<$LAST_NAME_GROUP>(\p{Ll}+\s+)*\p{Lu}\p{Ll}+(-\p{Lu}\p{Ll}+)?(\s+\p{LC}+\.)?)$""").toRegex()
    }
}
