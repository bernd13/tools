@file:SuppressWarnings("TooManyFunctions")
package de.praxisit.tools.common.calc

import java.text.DecimalFormatSymbols
import kotlin.jvm.Throws
import kotlin.math.absoluteValue
import kotlin.math.pow
import kotlin.math.sign

/**
 * Represents a Rational as a Quotient.
 * @property gcd Long greatest common divisor
 * @property numerator Long numerator of the Quotient
 * @property denominator Long denominator, must not be 0
 * @property isZero Boolean ´true` if numerator is 0
 * @property rational Double representation of the Quotient as a Double
 * @constructor construct Quotient eitehr with `Long`s or `Int`s
 */
public class Quotient(num: Long, denom: Long) : Comparable<Quotient> {
    init {
        require(denom != 0L)
    }

    private val gcd = gcd(num, denom) * denom.sign

    public val numerator: Long = (num / gcd)
    public val denominator: Long = denom / gcd

    public constructor(num: Int, denom: Int) : this(num.toLong(), denom.toLong())

    public val isZero: Boolean = numerator == 0L
    public val rational: Double = num.toDouble() / denom.toDouble()

    override fun toString(): String = if (denominator == 1L) "$numerator" else "$numerator / $denominator"

    override fun equals(other: Any?): Boolean = when {
        other == null -> false
        other === this -> true
        other is Int -> other.toQuotient() == this
        other is Long -> other.toQuotient() == this
        other !is Quotient -> false
        else -> this.numerator == other.numerator && this.denominator == other.denominator
    }

    override fun hashCode(): Int = (numerator xor denominator).toInt()

    override fun compareTo(other: Quotient): Int =
        (this.numerator * other.denominator - other.numerator * this.denominator).toInt()

    public operator fun plus(other: Quotient): Quotient = Quotient(
        this.numerator * other.denominator + other.numerator * this.denominator,
        this.denominator * other.denominator
    )

    public operator fun plus(other: Int): Quotient = Quotient(numerator + other * denominator, denominator)

    public operator fun plus(other: Long): Quotient = Quotient(numerator + other * denominator, denominator)

    public operator fun minus(other: Quotient): Quotient = Quotient(
        this.numerator * other.denominator - other.numerator * this.denominator,
        this.denominator * other.denominator
    )

    public operator fun minus(other: Int): Quotient =
        Quotient(numerator - other * denominator, denominator)

    public operator fun minus(other: Long): Quotient =
        Quotient(numerator - other * denominator, denominator)


    public operator fun times(other: Quotient): Quotient = Quotient(
        this.numerator * other.numerator, this.denominator * other.denominator
    )

    public operator fun times(other: Long): Quotient = Quotient(other * numerator, denominator)

    public operator fun times(other: Int): Quotient = Quotient(other * numerator, denominator)

    public operator fun div(other: Quotient): Quotient =
        if (other.isZero) throw IllegalArgumentException("Division by zero")
        else Quotient(this.numerator * other.denominator, other.numerator * this.denominator)

    public operator fun div(other: Long): Quotient = Quotient(numerator, other * denominator)

    public operator fun div(other: Int): Quotient = Quotient(numerator, other * denominator)

    public operator fun unaryMinus(): Quotient = Quotient(-numerator, denominator)

    /**
     * The reciprocal of the Quotient (exchange numerator and denominator).
     * @return Quotient the reciprocal Quotient
     */
    @Throws(IllegalArgumentException::class)
    public fun reciprocal(): Quotient = Quotient(this.denominator, this.numerator)

    public operator fun component1(): Long = numerator
    public operator fun component2(): Long = denominator

    internal companion object {
        fun gcd(n1: Long, n2: Long): Long {
            tailrec fun innerGcd(n1: Long, n2: Long): Long =
                if (n2 == 0L) n1 else innerGcd(n2, n1 % n2)

            return innerGcd(n1.absoluteValue, n2.absoluteValue)
        }
    }
}

public operator fun Long.plus(other: Quotient): Quotient =
    Quotient(this * other.denominator + other.numerator, other.denominator)

public operator fun Int.plus(other: Quotient): Quotient =
    Quotient(this * other.denominator + other.numerator, other.denominator)

public operator fun Long.minus(other: Quotient): Quotient =
    Quotient(this * other.denominator - other.numerator, other.denominator)

public operator fun Int.minus(other: Quotient): Quotient =
    Quotient(this * other.denominator - other.numerator, other.denominator)

public operator fun Long.times(other: Quotient): Quotient = Quotient(this * other.numerator, other.denominator)

public operator fun Int.times(other: Quotient): Quotient = Quotient(this * other.numerator, other.denominator)

public operator fun Long.div(other: Quotient): Quotient = Quotient(this * other.denominator, other.numerator)

public operator fun Int.div(other: Quotient): Quotient = Quotient(this * other.denominator, other.numerator)

/**
 * Convert Long to [Quotient].
 * @receiver Long Long to convert
 * @return Quotient Long as Quotient
 */
public fun Long.toQuotient(): Quotient = Quotient(this, 1L)

/**
 * Convert Int to [Quotient].
 * @receiver Int Int to convert
 * @return Quotient Int as Quotient
 */
public fun Int.toQuotient(): Quotient = Quotient(this, 1)

/**
 * Convert Double to [Quotient].
 * @receiver Double Double to convert
 * @return Quotient Double as Quotient
 */
public fun Double.toQuotient(): Quotient {
    fun fractionLength() =
        toString().substringAfter(DecimalFormatSymbols.getInstance().decimalSeparator).length.toDouble()

    val denom = TEN_BASE.pow(fractionLength()).toLong()
    val num = (this * denom).toLong()

    return Quotient(num, denom)
}

private const val TEN_BASE: Double = 10.0
