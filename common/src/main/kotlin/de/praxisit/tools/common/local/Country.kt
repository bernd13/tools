package de.praxisit.tools.common.local

import java.util.*

/**
 * Base class for countries..
 * @property locale [Locale]
 * @property iso2Countries Mapping of [ISO2-Code](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes)
 *      to the corresponding country
 * @property iso3Countries Mapping of [ISO3-Code](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes)
 *      to the corresponding country
 * @constructor Create a country from [Locale]
 */
public sealed class Country(private val locale: Locale) {
    private val iso2Countries: Map<String, String> =
        ISO_LAENDER.mapValues { it.value.getDisplayName(locale) }

    private val iso3Countries: Map<String, String> =
        ISO_LAENDER.values.associateBy({ it.isO3Country }, { it.getDisplayName(locale) })

    public fun iso2ToName(isoCode: String): String = iso2Countries[isoCode.toUpperCase()] ?: UNBEKANNTES_LAND
    public fun iso3ToName(isoCode: String): String = iso3Countries[isoCode.toUpperCase()] ?: UNBEKANNTES_LAND

    internal companion object {
        private val ISO_LAENDER = Locale.getISOCountries().associate { it to Locale("", it) }
        private const val UNBEKANNTES_LAND = "Unbekannt"
    }
}

public class LocaleCountry(locale: Locale) : Country(locale)
public object Deutschland : Country(Locale.GERMAN)


public class LandIso3Code(newCode: String) {
    private val code = newCode.toUpperCase()

    init {
        require(ISO_CODE_PATTERN.matches(code))
        require(code in CODES)
    }

    override fun toString(): String = code

    internal companion object {
        private val CODES = Locale.getISOCountries().map { Locale("", it).isO3Country }.toSet()
        private val ISO_CODE_PATTERN = "[A-Z]{3}".toRegex()
    }
}

public fun String.iso2ToName(country: Country = Deutschland): String = country.iso2ToName(this)
public fun String.iso3ToName(country: Country = Deutschland): String = country.iso3ToName(this)
