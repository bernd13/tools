@file:Suppress("RedundantVisibilityModifier")

package de.praxisit.tools.common.collection

/**
 * Split a list when the next element is different from its predecessor.
 *
 * @author Bernd Kursawe
 * @since 1.0.0
 * @receiver List<T> any list of elements of type <code>T</code>
 * @return a list of lists of type T
 */
public fun <T> List<T>.splitWhenChange(): List<List<T>> = this.splitWhenChangeBy { prev, next -> prev != next }

/**
 * Split a list when the [splitCondition] for the next element ist `true`.
 *
 * @author Bernd Kursawe
 * @since 1.0.0
 * @receiver List<T> any list of elements of type <code>T</code>
 * @return a list of lists of type T
 */
public fun <T> List<T>.splitWhenChangeBy(splitCondition: (prev: T, next: T) -> Boolean): List<List<T>> {
    if (this.isEmpty()) return emptyList()

    val iterator = this.iterator()
    val firstElement = iterator.next()
    val result = mutableListOf<List<T>>()
    var currentList = mutableListOf(firstElement)
    var lastElement = firstElement
    while (iterator.hasNext()) {
        val element = iterator.next()
        if (splitCondition(lastElement, element)) {
            result.add(currentList)
            currentList = mutableListOf(element)
        } else {
            currentList.add(element)
        }
        lastElement = element
    }
    result.add(currentList)
    return result.toList()
}
