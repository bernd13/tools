@file:Suppress("RedundantVisibilityModifier")

package de.praxisit.tools.common.collection

/**
 * Reduces a sequence of equal elements in a list to one.
 * An empty list return an empty list.
 *
 * @author Bernd Kursawe
 * @since 1.0.0
 * @sample [`Remove sequence duplicates from an empty list`]
 * @receiver List<T> of elements of type `T`
 * @return  a shorter list where every sequence of one element is reduced to only one occurence
 */
public fun <T> List<T>.removeSequenceDuplicates(): List<T> {
    val result = mutableListOf<T>()
    var currentElement: T? = null
    for (element in this) {
        if (element != currentElement) {
            result.add(element)
            currentElement = element
        }
    }
    return result.toList()
}
