@file:Suppress("ClassName")

package de.praxisit.tools.common.time

import de.praxisit.tools.common.time.YearMonthRange.Companion.compress
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import java.time.YearMonth
import java.time.format.DateTimeParseException

internal class YearMonthRangeTest {

    @Nested
    inner class `Create YearMonthRange with of` {
        @Test
        fun `Create correct range`() {
            val range = YearMonthRange.of(YearMonth.of(2020, 10), YearMonth.of(2020, 12))

            assertThat(range.start).isEqualTo(YearMonth.of(2020, 10))
            assertThat(range.endInclusive).isEqualTo(YearMonth.of(2020, 12))
        }

        @Test
        fun `Create range with same month for start and endInclusive`() {
            val range = YearMonthRange.of(YearMonth.of(2020, 10), YearMonth.of(2020, 10))

            assertThat(range.start).isEqualTo(YearMonth.of(2020, 10))
            assertThat(range.endInclusive).isEqualTo(YearMonth.of(2020, 10))
        }

        @Test
        fun `Create range with 1 month`() {
            val range = YearMonthRange.of(YearMonth.of(2020, 9))

            assertThat(range.start).isEqualTo(YearMonth.of(2020, 9))
            assertThat(range.endInclusive).isEqualTo(YearMonth.of(2020, 9))
        }

        @Test
        fun `Create range with start greater endInclusive`() {
            assertThatThrownBy {
                YearMonthRange.of(YearMonth.of(2020, 12), YearMonth.of(2020, 10))
            }
                .isInstanceOf(IllegalArgumentException::class.java)
        }
    }

    @Nested
    inner class `To string` {
        @ParameterizedTest
        @CsvSource(
            "2020, 9, 2021, 3, 09.2020 - 03.2021",
            "1877, 5, 2311, 8, 05.1877 - 08.2311",
            "333, 3, 8888, 8, 03.0333 - 08.8888",
            "1, 1, 2, 2, 01.0001 - 02.0002",
            "0, 1, 1, 1, 01.0001 - 01.0001",
            "-10, 4, 1, 1, 04.0011 - 01.0001",
            "2020, 1, 9999, 12, 01.2020 - 12.9999",
            "2020, 1, 10000, 1, 01.2020 - 01.+10000"
        )
        fun `convert to String`(year1: Int, month1: Int, year2: Int, month2: Int, result: String) {
            val date1 = YearMonth.of(year1, month1)
            val date2 = YearMonth.of(year2, month2)
            val range = YearMonthRange.of(date1, date2).toString()

            assertThat(range).isEqualTo(result)
        }
    }

    @Nested
    inner class `Equals Test` {
        @Test
        fun `equal with equal`() {
            val range1 = YearMonthRange.of(YearMonth.of(2020, 1), YearMonth.of(2020, 12))
            val range2 = YearMonthRange.of(YearMonth.of(2020, 1), YearMonth.of(2020, 12))

            val result = range1 == range2
            assertThat(result).isTrue
        }

        @Test
        fun `equal with same`() {
            val range1 = YearMonthRange.of(YearMonth.of(2020, 1), YearMonth.of(2020, 12))

            val result = range1 == range1
            assertThat(result).isTrue
        }

        @Test
        fun `equal with different`() {
            val range1 = YearMonthRange.of(YearMonth.of(2020, 1), YearMonth.of(2020, 11))
            val range2 = YearMonthRange.of(YearMonth.of(2020, 1), YearMonth.of(2020, 12))

            val result = range1 == range2
            assertThat(result).isFalse
        }

        @Test
        fun `equal with null`() {
            val range1: YearMonthRange = YearMonthRange.of(YearMonth.of(2020, 1), YearMonth.of(2020, 11))
            val range2: YearMonthRange? = null

            val result = range1 == range2
            assertThat(result).isFalse
        }

        @Test
        fun `equal with other data type`() {
            val range1: YearMonthRange = YearMonthRange.of(YearMonth.of(2020, 1), YearMonth.of(2020, 11))
            val range2: Any? = YearMonth.of(2020, 1)

            val result = range1 == range2
            assertThat(result).isFalse
        }
    }

    @Nested
    inner class `Convert String to YearMonth` {
        @Test
        fun `convert correct`() {
            val month = "2020-10".toYM

            assertThat(month).isEqualTo(YearMonth.of(2020, 10))
        }

        @ParameterizedTest(name = """convert "{0}" with exception""")
        @CsvSource(
            "2020-13",
            "2020-1",
            "2020-123",
            "abc",
            "''"
        )
        fun `convert with exception`(str: String) {
            assertThatThrownBy { str.toYM }.isInstanceOf(DateTimeParseException::class.java)
        }
    }

    @Nested
    inner class `Convert pair of strings to YearMonthRange` {
        @Test
        fun `convert correct with Pair`() {
            val range = ("2020-01" to "2020-03").toRange

            assertThat(range.start).isEqualTo("2020-01".toYM)
            assertThat(range.endInclusive).isEqualTo("2020-03".toYM)
        }

        @ParameterizedTest
        @CsvSource(
            "2020-03,2020-01",
            "2020-03,2020-1",
            "'',2020-1",
        )
        fun `convert with Pair ends up with exception`(start: String, end: String) {
            assertThatThrownBy {
                (start to end).toRange
            }.isInstanceOfAny(java.lang.IllegalArgumentException::class.java, DateTimeParseException::class.java)
        }

        @Test
        fun `convert correct with toMonth`() {
            val range = "2020-03" toMonth "2020-09"

            assertThat(range.start).isEqualTo("2020-03".toYM)
            assertThat(range.endInclusive).isEqualTo("2020-09".toYM)
        }

        @ParameterizedTest
        @CsvSource(
            "2020-03,2020-01",
            "2020-03,2020-1",
            "'',2020-1",
        )
        fun `convert with toMonth ends up with exception`(start: String, end: String) {
            assertThatThrownBy {
                start toMonth end
            }.isInstanceOfAny(java.lang.IllegalArgumentException::class.java, DateTimeParseException::class.java)
        }
    }

    @Nested
    inner class `Equals and hashCode` {
        @Test
        fun `equals for same ranges`() {
            val range1 = ("2020-01" to "2020-12").toRange
            val range2 = ("2020-01" to "2020-12").toRange

            assertThat(range1).isEqualTo(range2)
        }

        @Test
        fun `equals for different ranges`() {
            val range1 = ("2020-01" to "2020-12").toRange
            val range2 = ("2020-02" to "2020-12").toRange

            assertThat(range1).isNotEqualTo(range2)
        }

        @Test
        fun `hashCode for same ranges`() {
            val range1 = ("2021-01" to "2021-12").toRange
            val range2 = ("2021-01" to "2021-12").toRange

            assertThat(range1.hashCode()).isEqualTo(range2.hashCode())
        }

        @Test
        fun `hashCode for different ranges`() {
            val range1 = ("2021-01" to "2021-12").toRange
            val range2 = ("2021-01" to "2021-11").toRange

            assertThat(range1.hashCode()).isNotEqualTo(range2.hashCode())
        }
    }

    @Nested
    inner class Contains {
        @ParameterizedTest
        @CsvSource(
            "2020-08,2020-12,2020-10,true", // month in range
            "2020-08,2020-12,2020-08,true",  // month at begin of range
            "2020-08,2020-12,2020-12,true", // month at end of range
            "2020-08,2020-12,2020-07,false", // month before range
            "2020-08,2020-12,2021-01,false", // month after range
        )
        fun `month in range`(startStr: String, endStr: String, monthStr: String, result: Boolean) {
            val start = startStr.toYM
            val end = endStr.toYM
            val range = YearMonthRange.of(start, end)
            val month = monthStr.toYM

            assertThat(month in range).isEqualTo(result)
        }
    }

    @Nested
    inner class ToString {
        @ParameterizedTest
        @CsvSource(
            "2020-01",
            "2020-12",
            "1900-01",
            "2100-11",
        )
        fun `test toString`(ymStr: String) {
            assertThat(ymStr.toYM.toString()).isEqualTo(ymStr)
        }
    }

    @Nested
    inner class IteratorTest {
        @Test
        fun `iterate over a range`() {
            val range = "2020-05" toMonth "2022-04"

            val months = range.toSet()

            assertThat(months)
                .hasSize(24)
                .containsAnyOf("2020-05".toYM, "2022-04".toYM, "2021-01".toYM)
                .doesNotContain("2020-04".toYM, "2022-05".toYM)
        }

        @Test
        fun `iterate over a single month`() {
            val range = "2020-06".toRange

            val months = range.toSet()

            assertThat(months)
                .hasSize(1)
                .containsAnyOf("2020-06".toYM)
                .doesNotContain("2020-05".toYM, "2022-07".toYM)
        }
    }

    @Nested
    inner class Overlap {
        @ParameterizedTest
        @CsvSource(
            "2020-02,2020-06,2020-06,2020-09", // range1 overlaps the beginning of range2
            "2020-08,2020-12,2020-06,2020-09", // range1 overlaps the end of range2
            "2020-08,2020-12,2020-06,2020-09", // range1 overlaps the the complete range2
            "2020-08,2020-12,2020-06,2020-09", // range1 lies completely in range2
            "2020-08,2020-12,2020-08,2020-12", // range1 is equal to range2
        )
        fun `2 ranges that overlap`(start1: String, end1: String, start2: String, end2: String) {
            val range1 = start1 toMonth end1
            val range2 = start2 toMonth end2

            assertThat(range1 overlap range2).isTrue
        }

        @Test
        fun `2 ranges does not overlap`() {
            val range1 = "2020-02" toMonth "2020-06"
            val range2 = "2020-07" toMonth "2020-09"

            assertThat(range1 overlap range2).isFalse
            assertThat(range2 overlap range1).isFalse
        }
    }

    @Nested
    inner class PlusMonth {
        @Test
        fun `add the successor month of a range`() {
            val range = "2020-02" toMonth "2020-10"
            val month = "2020-11".toYM

            assertThat(range + month).isEqualTo("2020-02" toMonth "2020-11")
        }

        @Test
        fun `add the predecessor month of a range`() {
            val range = "2020-02" toMonth "2020-10"
            val month = "2020-01".toYM

            assertThat(range + month).isEqualTo("2020-01" toMonth "2020-10")
        }

        @Test
        fun `add a month of the range to the range`() {
            val range = "2020-02" toMonth "2020-10"
            val month = "2020-07".toYM

            assertThat(range + month).isEqualTo("2020-02" toMonth "2020-10")
        }

        @Test
        fun `add a month not belonging to the range result in the range`() {
            val range = "2020-02" toMonth "2020-10"
            val month = "2020-12".toYM

            assertThat(range + month).isEqualTo("2020-02" toMonth "2020-10")
        }
    }

    @Nested
    inner class PlusRange {
        @ParameterizedTest
        @CsvSource(
            "2020-01,2020-03,2020-04,2020-06,2020-01,2020-06",
            "2020-09,2020-12,2020-04,2020-08,2020-04,2020-12",
            "2020-03,2020-06,2020-05,2020-08,2020-03,2020-08",
            "2020-07,2020-09,2020-05,2020-08,2020-05,2020-09",
            "2020-06,2020-11,2020-07,2020-08,2020-06,2020-11",
            "2020-01,2020-05,2020-09,2020-10,2020-01,2020-05",
        )
        fun `add another range`(start1: String, end1: String, start2: String, end2: String, resultStart: String, resultEnd: String) {
            val range1 = start1 toMonth end1
            val range2 = start2 toMonth end2
            val result = resultStart toMonth resultEnd

            assertThat(range1 + range2).isEqualTo(result)
        }
    }

    @Nested
    inner class Compress {
        @Test
        fun `empty set creates empty compress list`() {
            val list = emptySet<YearMonth>()

            val result = list.compress()

            assertThat(result).isEmpty()
        }

        @Test
        fun `set with single month creates a compressed list with one range`() {
            val list = setOf("2020-07".toYM)

            val result = list.compress()

            assertThat(result).isEqualTo(listOf("2020-07".toRange))
        }

        @Test
        fun `set with a sequence of months creates a compressed list with one range`() {
            val set = setOf("2020-08", "2020-09", "2020-10").toYM

            val result = set.compress()

            assertThat(result).isEqualTo(listOf("2020-08" toMonth "2020-10"))
        }

        @Test
        fun `set with some single months creates a compressed list with the same number of ranges`() {
            val set = setOf("2020-06", "2020-08", "2020-10").toYM

            val result = set.compress()

            assertThat(result).containsExactly("2020-06".toRange, "2020-08".toRange, "2020-10".toRange)
        }

        @Test
        fun `set with some sequences of months creates a compressed list with the same number of ranges`() {
            val set = setOf("2020-06", "2020-07", "2020-08", "2020-10", "2020-12", "2021-01").toYM

            val result = set.compress()

            assertThat(result).containsExactly("2020-06" toMonth "2020-08", "2020-10".toRange, "2020-12" toMonth "2021-01")
        }

        @Test
        fun `set with some sequences of months creates a compressed list with a combined range`() {
            val set = setOf("2020-06", "2020-07", "2020-08", "2020-10", "2020-12", "2020-11", "2020-09").toYM

            val result = set.compress()

            assertThat(result).containsExactly("2020-06" toMonth "2020-12")
        }
    }
}
