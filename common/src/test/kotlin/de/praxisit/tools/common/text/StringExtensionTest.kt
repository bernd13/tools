package de.praxisit.tools.common.text

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class StringExtensionTest {
    @Nested
    inner class SuffixStringTest {
        @ParameterizedTest
        @CsvSource(
            "text,ext,text",
            "has.ext,.ext,has.ext",
            "has.xxx,.ext,has.xxx.ext",
            "'',.ext,.ext",
            "text,'',text"
        )
        fun `add suffix if it is not already there`(original: String, suffix: String, result: String) {
            assertThat(original.addSuffix(suffix)).isEqualTo(result)
        }
    }

    @Nested
    inner class SuffixCharSequenceTest {
        @ParameterizedTest
        @CsvSource(
            "text,ext,text",
            "has.ext,.ext,has.ext",
            "has.xxx,.ext,has.xxx.ext",
            "'',.ext,.ext",
            "text,'',text"
        )
        fun `add suffix if it is not already there`(original: String, suffix: CharSequence, result: String) {
            assertThat(original.addSuffix(suffix)).isEqualTo(result)
        }
    }

}