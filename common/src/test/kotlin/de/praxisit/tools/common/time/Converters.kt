@file:Suppress("unused")

package de.praxisit.tools.common.time

import java.time.YearMonth

val String.toYM: YearMonth
    get() = YearMonth.parse(this)

val Set<String>.toYM
    get() = this.map { it.toYM }.toSet()

val List<String>.toYM
    get() = this.map { it.toYM }.toList()

val Pair<String, String>.toRange
    get() = YearMonthRange.of(first.toYM, second.toYM)

val String.toRange
    get() = YearMonthRange.of(this.toYM)

infix fun String.toMonth(end: String) = YearMonthRange.of(this.toYM, end.toYM)
