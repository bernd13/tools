package de.praxisit.tools.common.collection

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class SplitTest {

    @Nested
    inner class SplitWhenChange {
        @Test
        fun `Split empty list`() {
            val emptyList = emptyList<String>()

            val splitList = emptyList.splitWhenChange()

            assertThat(splitList).isEmpty()
        }

        @Test
        fun `Split list with 1 element`() {
            val simpleList = listOf(1)

            val splitList = simpleList.splitWhenChange()

            assertThat(splitList)
                .hasSize(1)
                .contains(listOf(1))
        }

        @Test
        fun `Split list with some equal elements`() {
            val list = listOf(1, 1, 1)

            val splitList = list.splitWhenChange()

            assertThat(splitList)
                .hasSize(1)
                .contains(listOf(1, 1, 1))
        }

        @Test
        fun `Split list with 2 unequal elements`() {
            val list = listOf(1, 2)

            val splitList = list.splitWhenChange()

            assertThat(splitList)
                .hasSize(2)
                .contains(listOf(1), listOf(2))
        }

        @Test
        fun `Split list with some sequences of equal and unequal elements`() {
            val list = listOf(1, 2, 2, 2, 3, 3, 4, 5, 5)

            val splitList = list.splitWhenChange()

            assertThat(splitList)
                .hasSize(5)
                .contains(
                    listOf(1),
                    listOf(2, 2, 2),
                    listOf(3, 3),
                    listOf(4),
                    listOf(5, 5)
                )
        }
    }

    @Nested
    inner class SplitWhenChangeBy {
        @Test
        fun `split an empty list`() {
            val list = emptyList<Int>()

            val result = list.splitWhenChangeBy(::notBeSameOrNext)

            assertThat(result).isEmpty()
        }

        @Test
        fun `split a list with 1 element`() {
            val list = listOf(1)

            val result = list.splitWhenChangeBy(::notBeSameOrNext)

            assertThat(result).isEqualTo(listOf(listOf(1)))
        }

        @Test
        fun `split a list with some same elements`() {
            val list = listOf(1, 1, 2, 3)

            val result = list.splitWhenChangeBy(::notBeSameOrNext)

            assertThat(result).isEqualTo(listOf(listOf(1, 1, 2, 3)))
        }

        @Test
        fun `split a list with different elements`() {
            val list = listOf(1, 3, 5)

            val result = list.splitWhenChangeBy(::notBeSameOrNext)

            assertThat(result).isEqualTo(listOf(listOf(1), listOf(3), listOf(5)))
        }

        @Test
        fun `split a list with some same and some different elements`() {
            val list = listOf(1, 1, 2, 4, 5, 6, 9)

            val result = list.splitWhenChangeBy(::notBeSameOrNext)

            assertThat(result).isEqualTo(listOf(listOf(1, 1, 2), listOf(4, 5, 6), listOf(9)))
        }

        private fun notBeSameOrNext(prev: Int, next: Int) = (next > prev + 1)
    }
}