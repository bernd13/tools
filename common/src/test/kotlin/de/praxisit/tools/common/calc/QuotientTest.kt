package de.praxisit.tools.common.calc

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import kotlin.math.sign

internal class QuotientTest {

    @Nested
    inner class ConstructQuotient {
        @Test
        fun `construct Quotient`() {
            val q = Quotient(2, 3)

            assertThat(q.numerator).isEqualTo(2)
            assertThat(q.denominator).isEqualTo(3)
        }

        @Test
        fun `construct and simplify Quotient`() {
            val q = Quotient(12, 15)

            assertThat(q.numerator).isEqualTo(4)
            assertThat(q.denominator).isEqualTo(5)
        }

        @ParameterizedTest
        @CsvSource(
            "-3, 5, -3, 5",
            "3, -5, -3, 5",
            "-3, -5, 3, 5",
        )
        fun `consturct a negative Quotient`(num: Long, denom: Long, expectedNum: Long, expectedDenom: Long) {
            val q = Quotient(num, denom)

            assertThat(q.numerator).isEqualTo(expectedNum)
            assertThat(q.denominator).isEqualTo(expectedDenom)
        }

        @Test
        fun `construct with 0 denominator`() {
            assertThatThrownBy { Quotient(1, 0) }.isExactlyInstanceOf(IllegalArgumentException::class.java)
        }
    }

    @Nested
    inner class ComputeGcd {
        @ParameterizedTest
        @CsvSource(
            "1, 99, 1",
            "17, 19, 1",
            "3, 3, 3",
            "12, 16, 4",
            "34, 51, 17",
            "95, 38, 19",
            "-2, 4, 2",
            "0, 5, 5",
            "5, 0, 5",
        )
        fun `compute greatest common divisor`(num1: Long, num2: Long, expected: Long) {
            val gcd = Quotient.gcd(num1, num2)

            assertThat(gcd).isEqualTo(expected)
        }
    }

    @Nested
    inner class ToString {
        @ParameterizedTest
        @CsvSource(
            "1, 2, 1 / 2",
            "4, 3, 4 / 3",
            "-1, 2, -1 / 2",
            "8, 2, 4",
            "0, 2, 0",
        )
        fun `show Quotient as String`(num: Long, denom: Long, expected: String) {
            val q = Quotient(num, denom)

            assertThat(q.toString()).isEqualTo(expected)
        }
    }

    @Nested
    inner class Equals {
        @ParameterizedTest
        @CsvSource(
            "1, 2, 2, 4, true",
            "2, 5, 2, 5, true",
            "3, 6, 4, 8, true",
            "0, 7, 0, 3, true",
            "4, 5, 3, 4, false",
            "-1, 2, 1, 2, false",
        )
        fun `equals 2 Quotients`(num1: Long, denom1: Long, num2: Long, denom2: Long, expected: Boolean) {
            val q1 = Quotient(num1, denom1)
            val q2 = Quotient(num2, denom2)

            val result = q1 == q2

            assertThat(result).isEqualTo(expected)
        }

        @ParameterizedTest
        @CsvSource(
            "6, 1, 6, true",
            "8, 2, 4, true",
            "0, 3, 0, true",
            "-1, 1, -1, true",
            "7, 6, 1, false",
            "-4, 5, -1, false"
        )
        fun `equals Quotient and Int`(num: Long, denom: Long, int: Int, result: Boolean) {
            assertThat(Quotient(num, denom).equals(int)).isEqualTo(result)
        }

        @ParameterizedTest
        @CsvSource(
            "6, 1, 6, true",
            "8, 2, 4, true",
            "0, 3, 0, true",
            "-1, 1, -1, true",
            "7, 6, 1, false",
            "-4, 5, -1, false"
        )
        fun `equals Quotient and Long`(num: Long, denom: Long, long: Long, result: Boolean) {
            assertThat(Quotient(num, denom).equals(long)).isEqualTo(result)
        }
    }

    @Nested
    inner class HashCode {
        @Test
        fun `compute same hashCode`() {
            val q1 = Quotient(3, 5)
            val q2 = Quotient(9, 15)

            assertThat(q1.hashCode()).isEqualTo(q2.hashCode())
        }

        @Test
        fun `compute different hashCode`() {
            val q1 = Quotient(3, 5)
            val q2 = Quotient(10, 15)

            assertThat(q1.hashCode()).isNotEqualTo(q2.hashCode())
        }
    }

    @Nested
    inner class CompareTo {
        @ParameterizedTest
        @CsvSource(
            "1, 2, 1, 3, 1",
            "3, 2, 3, 4, 1",
            "4, 5, 8, 10, 0",
            "7, 8, 15, 16, -1",
            "3, 2, 5, 3, -1",
            "0, 1, 2, 3, -1",
        )
        fun `compare 2 Quotients`(num1: Long, denom1: Long, num2: Long, denom2: Long, expected: Long) {
            val q1 = Quotient(num1, denom1)
            val q2 = Quotient(num2, denom2)

            val result1 = q1.compareTo(q2)
            val result2 = q2.compareTo(q1)

            assertThat(result1.sign).isEqualTo(expected)
            assertThat(result2.sign).isEqualTo(-expected)
        }
    }

    @Nested
    inner class Operators {
        @Nested
        inner class Plus {
            @ParameterizedTest
            @CsvSource(
                "1, 2, 3, 4, 5, 4",
                "2, 7, 3, 7, 5, 7",
                "5, 9, -1, 3, 2, 9",
                "3, 4, 0, 5, 3, 4",
            )
            fun `add 2 Quotients`(
                num1: Long,
                denom1: Long,
                num2: Long,
                denom2: Long,
                expectedNum: Long,
                expectedDenom: Long
            ) {
                val q1 = Quotient(num1, denom1)
                val q2 = Quotient(num2, denom2)

                val result = q1 + q2

                assertThat(result.numerator).isEqualTo(expectedNum)
                assertThat(result.denominator).isEqualTo(expectedDenom)
            }
        }

        @Nested
        inner class Minus {
            @ParameterizedTest
            @CsvSource(
                "1, 2, 3, 4, -1, 4",
                "3, 7, 2, 7, 1, 7",
                "5, 9, -1, 3, 8, 9",
                "3, 4, 0, 5, 3, 4",
            )
            fun `subtract 2 Quotients`(
                num1: Long,
                denom1: Long,
                num2: Long,
                denom2: Long,
                expectedNum: Long,
                expectedDenom: Long
            ) {
                val q1 = Quotient(num1, denom1)
                val q2 = Quotient(num2, denom2)

                val result = q1 - q2

                assertThat(result.numerator).isEqualTo(expectedNum)
                assertThat(result.denominator).isEqualTo(expectedDenom)
            }
        }

        @Nested
        inner class Times {
            @ParameterizedTest
            @CsvSource(
                "1, 2, 3, 4, 3, 8",
                "2, 7, 3, 7, 6, 49",
                "5, 9, -1, 3, -5, 27",
                "3, 4, 0, 5, 0, 1",
            )
            fun `multiply 2 Quotients`(
                num1: Long,
                denom1: Long,
                num2: Long,
                denom2: Long,
                expectedNum: Long,
                expectedDenom: Long
            ) {
                val q1 = Quotient(num1, denom1)
                val q2 = Quotient(num2, denom2)

                val result = q1 * q2

                assertThat(result.numerator).isEqualTo(expectedNum)
                assertThat(result.denominator).isEqualTo(expectedDenom)
            }

        }

        @Nested
        inner class Div {
            @ParameterizedTest
            @CsvSource(
                "1, 2, 3, 4, 2, 3",
                "2, 7, 3, 7, 2, 3",
                "5, 9, -1, 3, -5, 3",
                "0, 4, 3, 5, 0, 1",
            )
            fun `divide 2 Quotients`(
                num1: Long,
                denom1: Long,
                num2: Long,
                denom2: Long,
                expectedNum: Long,
                expectedDenom: Long
            ) {
                val q1 = Quotient(num1, denom1)
                val q2 = Quotient(num2, denom2)

                val result = q1 / q2

                assertThat(result.numerator).isEqualTo(expectedNum)
                assertThat(result.denominator).isEqualTo(expectedDenom)
            }

            @Test
            fun `divide Quotient by 0`() {
                val q1 = Quotient(1, 2)
                val q2 = Quotient(0, 2)

                assertThatThrownBy { q1 / q2 }.isInstanceOf(IllegalArgumentException::class.java)
            }
        }

        @Nested
        inner class UnaryMinus {
            @Test
            fun `minus a positive Quotient`() {
                val q = Quotient(1, 2)

                assertThat(-q).isEqualTo(Quotient(-1, 2))
            }

            @Test
            fun `minus a negative Quotient`() {
                val q = Quotient(-1, 2)

                assertThat(-q).isEqualTo(Quotient(1, 2))
            }
        }

        @Nested
        inner class IntPlus {
            @Test
            fun `add Int and Quotient`() {
                val q = Quotient(2, 5)

                val result = 3 + q

                assertThat(result.numerator).isEqualTo(17)
                assertThat(result.denominator).isEqualTo(5)
            }

            @Test
            fun `add Quotient and Int`() {
                val q = Quotient(4, 7)

                val result = q + 4

                assertThat(result.numerator).isEqualTo(32)
                assertThat(result.denominator).isEqualTo(7)
            }
        }

        @Nested
        inner class LongPlus {
            @Test
            fun `add Long and Quotient`() {
                val q = Quotient(2, 5)

                val result = 5L + q

                assertThat(result.numerator).isEqualTo(27L)
                assertThat(result.denominator).isEqualTo(5L)
            }

            @Test
            fun `add Quotient and Long`() {
                val q = Quotient(4, 7)

                val result = q + 4L

                assertThat(result.numerator).isEqualTo(32)
                assertThat(result.denominator).isEqualTo(7)
            }
        }
    }

    @Nested
    inner class Functions {
        @Nested
        inner class IsZero {
            @Test
            fun `Quotient is zero`() {
                val q = Quotient(0, 3)

                assertThat(q.isZero).isTrue
            }

            @Test
            fun `Quotient is not zero`() {
                val q = Quotient(1, 3)

                assertThat(q.isZero).isFalse
            }
        }

        @Nested
        inner class Reciprocal {
            @Test
            fun `reciprocal of normal Quotient`() {
                val q = Quotient(2, 5)

                val result = q.reciprocal()

                assertThat(result).isEqualTo(Quotient(5, 2))
            }

            @Test
            fun `reciprocal of Quotient 1`() {
                val q = 1.toQuotient()

                val result = q.reciprocal()

                assertThat(result).isEqualTo(1.toQuotient())
            }

            @Test
            fun `reciprocal of Quotient 0`() {
                val q = 0.toQuotient()

                assertThatThrownBy {
                    q.reciprocal()
                }
            }
        }

        @Nested
        inner class Rational {
            @ParameterizedTest
            @CsvSource(
                "6, 5, 1.2",
                "5, 5, 1.0",
                "3, 12, 0.25",
                "-20, 8, -2.5",
                "0, 5, 0"
            )
            fun `convert double to Quotient`(num: Long, denom: Long, result: Double) {
                assertThat(Quotient(num, denom).rational).isEqualTo(result)
            }
        }
    }

    @Nested
    inner class Extensions {
        @Nested
        inner class IntToQuotient {
            @Test
            fun `positive integer to Quotient`() {
                val q = 3.toQuotient()

                assertThat(q.numerator).isEqualTo(3)
                assertThat(q.denominator).isEqualTo(1)
            }

            @Test
            fun `negative integer to Quotient`() {
                val q = (-2).toQuotient()

                assertThat(q.numerator).isEqualTo(-2)
                assertThat(q.denominator).isEqualTo(1)
            }

            @Test
            fun `zero integer to Quotient`() {
                val q = 0.toQuotient()

                assertThat(q.numerator).isEqualTo(0)
                assertThat(q.denominator).isEqualTo(1)
            }
        }

        @Nested
        inner class LongToQuotient {
            @Test
            fun `positive integer to Quotient`() {
                val q = 3L.toQuotient()

                assertThat(q.numerator).isEqualTo(3)
                assertThat(q.denominator).isEqualTo(1)
            }

            @Test
            fun `negative integer to Quotient`() {
                val q = (-2L).toQuotient()

                assertThat(q.numerator).isEqualTo(-2)
                assertThat(q.denominator).isEqualTo(1)
            }

            @Test
            fun `zero integer to Quotient`() {
                val q = 0L.toQuotient()

                assertThat(q.numerator).isEqualTo(0)
                assertThat(q.denominator).isEqualTo(1)
            }
        }

        @Nested
        inner class DoubleToQuotient {
            @Test
            fun `positive double to Quotient`() {
                val q = 3.2.toQuotient()

                assertThat(q.numerator).isEqualTo(16)
                assertThat(q.denominator).isEqualTo(5)
            }

            @Test
            fun `negative double to Quotient`() {
                val q = (-2.5).toQuotient()

                assertThat(q.numerator).isEqualTo(-5)
                assertThat(q.denominator).isEqualTo(2)
            }

            @Test
            fun `zero double to Quotient`() {
                val q = 0.0.toQuotient()

                assertThat(q.numerator).isEqualTo(0)
                assertThat(q.denominator).isEqualTo(1)
            }
        }
    }
}
