@file:Suppress("unused")

package de.praxisit.tools.common.generator

import java.util.*

class UuidGenerator : Generator<UUID> {
    override fun create(): UUID = UUID.randomUUID()
    override fun create(from: String): UUID = UUID.fromString(from)
}
