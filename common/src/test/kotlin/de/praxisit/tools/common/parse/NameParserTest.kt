package de.praxisit.tools.common.parse

import de.praxisit.tools.common.parse.ParsedName.Companion.parseName
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

internal class NameParserTest {
    @Nested
    inner class NameParser {
        @ParameterizedTest
        @CsvSource(
            "Bernd Kursawe,Bernd,Kursawe,",
            "Dr. Albert Einstein,Albert,Einstein,Dr.",
            "Hans-Werner Böhm,Hans-Werner,Böhm,",
            "Uwe Laß,Uwe,Laß,",
            "André Gäbel,André,Gäbel,",
            "Ölaf Änderson,Ölaf,Änderson,",
            "Annegret Kramp-Karrenbauer,Annegret,Kramp-Karrenbauer,",
            "Johan-Ándrei Weiß-Wûrst,Johan-Ándrei,Weiß-Wûrst,",
            "Françine Bohmans-Müller,Françine,Bohmans-Müller,",
            "Sammy Davis Jr.,Sammy,Davis Jr.,",
            "Prof. Dr. Ursula von der Leyen,Ursula,von der Leyen,Prof. Dr.",
            "Spock,,Spock,",
            "kleine Vogelpoth,,kleine Vogelpoth,",
            "   Bernd     Kursawe   ,Bernd,Kursawe,",
            ",,,",
            "'',,,",
            "123,,,"
        )
        fun `parse String to Name`(text: String?, firstName: String?, lastName: String?, title: String?) {
            val name = text.parseName()
            assertThat(name?.firstName).isEqualTo(firstName)
            assertThat(name?.lastName).isEqualTo(lastName)
            assertThat(name?.title).isEqualTo(title)
        }
    }
}
