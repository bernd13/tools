package de.praxisit.tools.common.local

import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

internal class CountryIso3CodeTest {
    @ParameterizedTest
    @CsvSource(
        "DEU",
        "FRA",
        "ITA",
        "usa",
        "Nld"
    )
    fun `Accept strings as CountryIso3Code`(code: String) {
        assertDoesNotThrow { LandIso3Code(code) }
    }

    @ParameterizedTest
    @CsvSource(
        "XXX",
        "DE",
        "''"
    )
    fun `IllegalArgumentException when wrong input`(code: String) {
        assertThrows<IllegalArgumentException> { LandIso3Code(code) }
    }
}
