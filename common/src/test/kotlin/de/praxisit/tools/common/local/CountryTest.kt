package de.praxisit.tools.common.local

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import java.util.*

internal class CountryTest {
    @ParameterizedTest
    @CsvSource(
        "DE,Deutschland",
        "US,Vereinigte Staaten",
        "FR,Frankreich"
    )
    fun `Find Countries in German with iso2ToName`(code: String, name: String) {
        assertThat(Deutschland.iso2ToName(code)).`as`("ISO2-Code: $code").isEqualTo(name)
    }

    @ParameterizedTest
    @CsvSource(
        "DEU,Deutschland",
        "ITA,Italien",
        "AUS,Australien"
    )
    fun `Find Countries in German with iso3ToName`(code: String, name: String) {
        assertThat(Deutschland.iso3ToName(code)).isEqualTo(name)
    }

    @ParameterizedTest
    @CsvSource(
        "DE,Deutschland",
        "ES,Spanien",
        "NO,Norwegen"
    )
    fun `Find Countries in German with the extension iso2ToName`(code: String, name: String) {
        assertThat(code.iso2ToName()).isEqualTo(name)
    }

    @ParameterizedTest
    @CsvSource(
        "DEU,Deutschland",
        "NLD,Niederlande",
        "BEL,Belgien"
    )
    fun `Find Countries in German with the extension iso3ToName`(code: String, name: String) {
        assertThat(code.iso3ToName()).isEqualTo(name)
    }

    @ParameterizedTest
    @CsvSource(
        "DE,Germany",
        "US,United States",
        "FR,France"
    )
    fun `Find Countries in English with iso2ToName`(code: String, name: String) {
        assertThat(LocaleCountry(Locale.ENGLISH).iso2ToName(code)).`as`("ISO2-Code: $code").isEqualTo(name)
    }

    @ParameterizedTest
    @CsvSource(
        "DEU,Germany",
        "ITA,Italy",
        "AUS,Australia"
    )
    fun `Find Countries in English with iso3ToName`(code: String, name: String) {
        assertThat(LocaleCountry(Locale.ENGLISH).iso3ToName(code)).isEqualTo(name)
    }
}
