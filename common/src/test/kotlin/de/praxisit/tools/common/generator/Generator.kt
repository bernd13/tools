package de.praxisit.tools.common.generator

interface Generator<T: Any> {
    fun create(): T
    fun create(from: String): T
}