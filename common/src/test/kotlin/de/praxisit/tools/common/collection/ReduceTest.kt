package de.praxisit.tools.common.collection

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class ReduceTest {

    @Nested
    inner class RemoveSequenceDuplicates {

        @Test
        fun `Remove sequence duplicates from an empty list`() {
            val list = emptyList<Int>()

            val result = list.removeSequenceDuplicates()

            assertThat(result).isEmpty()
        }

        @Test
        fun `Remove sequence duplicates from a list with a single element`() {
            val list = listOf(1)

            val result = list.removeSequenceDuplicates()

            assertThat(result).isEqualTo(listOf(1))
        }

        @Test
        fun `Remove sequence duplicates from a list with some equal elements`() {
            val list = listOf(2, 2, 2)

            val result = list.removeSequenceDuplicates()

            assertThat(result).isEqualTo(listOf(2))
        }

        @Test
        fun `Remove sequence duplicates from a list with all different elements`() {
            val list = listOf(1, 2, 3)

            val result = list.removeSequenceDuplicates()

            assertThat(result).isEqualTo(listOf(1, 2, 3))

        }

        @Test
        fun `Remove sequence duplicates from a list with equal and different elements`() {
            val list = listOf(1, 2, 2, 2, 3, 3, 4)

            val result = list.removeSequenceDuplicates()

            assertThat(result).isEqualTo(listOf(1, 2, 3, 4))

        }
    }
}