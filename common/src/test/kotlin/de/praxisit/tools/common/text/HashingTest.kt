package de.praxisit.tools.common.text

import de.praxisit.tools.common.text.Hashing.md5
import de.praxisit.tools.common.text.Hashing.sha1
import de.praxisit.tools.common.text.Hashing.sha256
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class HashingTest {
    @Nested
    inner class Md5Test {
        @Test
        fun `convert a string to MD5`() {
            val md5 = "This is a text".md5

            assertThat(md5).isEqualTo("0cd0a3afe6daaa52baf1874d56764e79")
        }
    }

    @Nested
    inner class Sha1Test {
        @Test
        fun `convert a string to SHA-1`() {
            val sha1 = "This is a text".sha1

            assertThat(sha1).isEqualTo("a1a8d5a209038469ba57d5505bfb8e1ff68a8c3b")
        }
    }

    @Nested
    inner class Sha256Test {
        @Test
        fun `convert a string to SHA-256`() {
            val sha256 = "This is a text".sha256

            assertThat(sha256).isEqualTo("1719b9ed2519f52da363bef16266c80c679be1c3ad3b481722938a8f1a9c589b")
        }
    }
}
