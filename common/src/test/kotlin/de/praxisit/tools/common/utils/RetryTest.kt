package de.praxisit.tools.common.utils

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class RetryTest {
    @Nested
    inner class Trials {
        @Test
        fun `successful retry some times`() {
            var i = 0

            val result = retry {
                if (++i < 3) throw IllegalArgumentException() else 13
            }

            assertThat(result).isEqualTo(13)
        }

        @Test
        fun `successful retry a lot of trials`() {
            var i = 0

            val result = retry {
                if (++i < DEFAULT_TRIES) throw IllegalArgumentException() else 13
            }

            assertThat(result).isEqualTo(13)
        }

        @Test
        fun `retry forever`() {
            assertThatThrownBy { retry { throw IllegalArgumentException() } }
                .isInstanceOf(IllegalArgumentException::class.java)
        }
    }
}