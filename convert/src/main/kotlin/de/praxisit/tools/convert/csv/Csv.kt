package de.praxisit.tools.convert.csv

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.csv.CsvMappingException
import com.fasterxml.jackson.dataformat.csv.CsvParser.Feature.IGNORE_TRAILING_UNMAPPABLE
import com.fasterxml.jackson.dataformat.csv.CsvSchema
import com.fasterxml.jackson.module.kotlin.KotlinModule
import java.io.FileReader
import java.io.FileWriter

public val csvMapper: CsvMapper = CsvMapper().apply {
    registerModule(KotlinModule())
}
    .configure(IGNORE_TRAILING_UNMAPPABLE, true)
    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    .configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true)
        as CsvMapper

/**
 * Read a file with csv-Format to a list of objects of type `T`.
 * @param fileName String absolute location of file
 * @return List<T> objects of type T
 * @exception CsvMappingException if an error occurs during deserialization
 */
public inline fun <reified T> readCsvFile(fileName: String): List<T> {
    FileReader(fileName).use { reader ->
        return csvMapper
            .readerFor(T::class.java)
            .with(CsvSchema.emptySchema().withNullValue("null").withHeader())
            .readValues<T>(reader)
            .readAll()
            .toList()
    }
}

/**
 * Write a csv file from a collection of objects of type `T`.
 * @receiver Collection<T> objects of type `T`
 * @param fileName String absolute path of the csv file
 */
public inline fun <reified T> Collection<T>.writeCsvFile(fileName: String) {
    FileWriter(fileName).use { writer ->
        csvMapper.writer(csvMapper.schemaFor(T::class.java).withHeader())
            .writeValues(writer)
            .writeAll(this)
            .close()
    }
}
