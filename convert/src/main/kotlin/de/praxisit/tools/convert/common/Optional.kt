package de.praxisit.tools.convert.common

import java.util.*

/**
 * Convert [Optional] of type `T` to a Kotlin nullable `T?`.
 *
 * @receiver Optional<T> Type `T`
 * @return T? nullable Type
 */
public val <T> Optional<T>.nullable: T?
    get() = if (this.isPresent) this.get() else null
