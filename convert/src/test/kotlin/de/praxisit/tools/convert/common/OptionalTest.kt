package de.praxisit.tools.convert.common

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

class OptionalTest {
    @Test
    fun `convert Optional with value to nullable`() {
        val opt = Optional.of("String")

        assertThat(opt.nullable).isEqualTo("String")
    }

    @Test
    fun `convert empty Optional to nullable`() {
        val opt = Optional.empty<String>()

        assertThat(opt.nullable).isNull()
    }
}