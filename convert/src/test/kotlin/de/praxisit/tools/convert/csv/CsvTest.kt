package de.praxisit.tools.convert.csv

import com.fasterxml.jackson.dataformat.csv.CsvMappingException
import de.praxisit.tools.common.text.addSuffix
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.io.TempDir
import java.io.FileReader
import java.io.FileWriter
import java.nio.file.Path

class CsvTest {

    @Nested
    inner class CsvReaderTest {
        @Test
        fun `read a normal csv to a class`(@TempDir tempDir: Path) {
            val csv = createPersonCsv(tempDir)

            val people = readCsvFile<Person>(csv)

            assertThat(people)
                .hasSize(3)
                .contains(Person("Donald", "Duck"))
                .contains(Person("Daniel", "Düsentrieb", "Dr."))
                .contains(Person("Track", ""))
        }

        @Test
        fun `read a normal csv to a map`(@TempDir tempDir: Path) {
            val csv = createPersonCsv(tempDir)

            val people = readCsvFile<Map<String, String?>>(csv)

            assertThat(people)
                .hasSize(3)
                .contains(mapOf("firstName" to "Donald", "lastName" to "Duck", "title" to null))
                .contains(mapOf("firstName" to "Daniel", "lastName" to "Düsentrieb", "title" to "Dr."))
                .contains(mapOf("firstName" to "Track", "lastName" to "", "title" to null))
        }

        @Test
        fun `read a file without data to a class`(@TempDir tempDir: Path) {
            val csv = createCsv(
                tempDir,
                "person",
                "firstName,lastName,title"
            )

            val people = readCsvFile<Person>(csv)

            assertThat(people).isEmpty()
        }

        @Test
        fun `read an empty file`(@TempDir tempDir: Path) {
            val csv = createCsv(
                tempDir,
                "person"
            )

            assertThrows<CsvMappingException> { readCsvFile<Person>(csv) }
        }

        @Test
        fun `read a file with missing columns`(@TempDir tempDir: Path) {
            val csv = createCsv(
                tempDir,
                "person",
                "firstName,lastName,title",
                "Donald,Duck,null",
                "Daniel,Düsentrieb,Dr.",
                "Track"
            )

            val people = readCsvFile<Person>(csv)

            assertThat(people)
                .hasSize(3)
                .contains(Person("Donald", "Duck"))
                .contains(Person("Daniel", "Düsentrieb", "Dr."))
                .contains(Person("Track", ""))
        }

        @Test
        fun `read file with more than necessary columns`(@TempDir tempDir: Path) {
            val csv = createCsv(
                tempDir,
                "person",
                "firstName,lastName,title,age",
                "Donald,Duck,null,50",
                "Daniel,Düsentrieb,Dr.,35",
                "Track,,null,10"
            )

            val people = readCsvFile<Person>(csv)

            assertThat(people)
                .hasSize(3)
                .contains(Person("Donald", "Duck"))
                .contains(Person("Daniel", "Düsentrieb", "Dr."))
                .contains(Person("Track", ""))
        }
    }

    @Nested
    inner class CsvWriterTest {
        @Test
        fun `write a file with some persons`(@TempDir tempDir: Path) {
            val crew = listOf(
                Person("James T.", "Kirk"),
                Person("Spock"),
                Person("Leonard", "McCoy", "Dr.")
            )
            val fileName = tempDir.resolve("enterprise.csv").toString()
            crew.writeCsvFile(fileName)
            
            val lines = readCsv(fileName)
            assertThat(lines)
                .hasSize(4)
                .contains("firstName,lastName,title")
                .contains(""""James T.",Kirk,""")
                .contains("Spock,,")
                .contains("Leonard,McCoy,Dr.")
        }

        @Test
        fun `write an file with no person`(@TempDir tempDir: Path) {
            val fileName = tempDir.resolve("enterprise.csv").toString()
            emptyList<Person>().writeCsvFile(fileName)

            val lines = readCsv(fileName)
            assertThat(lines)
                .hasSize(1)
                .contains("firstName,lastName,title")
        }

    }

    data class Person(
        val firstName: String?,
        val lastName: String? = "",
        val title: String? = null
    )

    companion object {
        fun createCsv(path: Path, fileName: String, vararg lines: String): String {
            val file = path.resolve(fileName.addSuffix("csv")).toString()
            FileWriter(file).use { writer ->
                lines.forEach { line ->
                    writer.append(line).append("\n")
                }
                writer.close()
            }
            return file
        }
        
        fun readCsv(fileName: String): List<String> {
            FileReader(fileName).use { reader ->
                return reader.readLines()
            }
        }

        fun createPersonCsv(tempDir: Path) = createCsv(
            tempDir,
            "person",
            "firstName,lastName,title",
            "Donald,Duck,null",
            "Daniel,Düsentrieb,Dr.",
            "Track,,null"
        )
    }

}